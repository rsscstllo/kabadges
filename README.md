# README #

## About This Repository ##

* An Android application that tells Khan Academy students what the different Challenge Patches represent.  
* Version 1.0.0  
* Deployment Target: 4.1 (Jelly Bean)  
* Devices: Universal  
* Device Orientation: Portrait and landscape  
* Android Studio 2.3.0, Java 1.8.0_111-b14   

## How To Set Up ##
* Download the project  
* Open Android Studio  
* Select **Open an existing Android Studio Project**  
* Select **KABadges** and click **OK**  
* Click play  

## Source Files ##

#### activities ####
* BadgeActivity.java  
* BadgeDetailActivity.java  

#### adapters ####
* BadgeAdapter.java  

#### models ####
* Badge.java  

#### network ####
* Connector.java  
* JSONDownloader.java  
* JSONParser.java  

#### layout ####
* activity_badge.xml  
* activity_badge_detail.xml  
* list_item_badge.xml  

## Reference ##
* [Picasso](https://github.com/square/picasso)  
* [Khan Academy API](https://api-explorer.khanacademy.org)  
