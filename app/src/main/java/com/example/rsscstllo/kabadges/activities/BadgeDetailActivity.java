package com.example.rsscstllo.kabadges.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rsscstllo.kabadges.R;
import com.squareup.picasso.Picasso;

/**
 * Created by rosscastillo on 3/6/17.
 */

public class BadgeDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_badge_detail);
        updateUI();
    }

    private void updateUI() {
        setTitle("Challenge Patch");
        getWindow().getDecorView().setBackgroundColor(Color.WHITE);

        String url = this.getIntent().getExtras().getString("url");
        String name = this.getIntent().getExtras().getString("name");
        String description = this.getIntent().getExtras().getString("description");

        ImageView badgeIconImageView = (ImageView) findViewById(R.id.badge_image);
        TextView badgeNameTextView = (TextView) findViewById(R.id.badge_name);
        TextView badgeDescriptionTextView = (TextView) findViewById(R.id.badge_description);

        Picasso.with(this)
                .load(url)
                .placeholder(R.mipmap.ic_launcher_round)
                .into(badgeIconImageView);
        badgeNameTextView.setText(name);
        badgeDescriptionTextView.setText(description);

        badgeNameTextView.setTextColor(Color.argb(255, 0, 0, 0));
        badgeDescriptionTextView.setTextColor(Color.argb(128, 0, 0, 0));
    }

}
