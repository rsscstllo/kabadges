package com.example.rsscstllo.kabadges.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.rsscstllo.kabadges.R;
import com.example.rsscstllo.kabadges.network.JSONDownloader;

/**
 * Created by rosscastillo on 3/6/17.
 */

public class BadgeActivity extends AppCompatActivity {

    private ListView badgeListView;
    private final String jsonURL = "https://www.khanacademy.org/api/v1/badges?format=pretty";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_badge);
        setTitle("Challenge Patches");
        badgeListView = (ListView) findViewById(R.id.badge_list_view);
        new JSONDownloader(BadgeActivity.this, jsonURL, badgeListView).execute();
    }
}
