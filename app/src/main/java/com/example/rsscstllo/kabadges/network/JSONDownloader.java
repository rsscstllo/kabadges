package com.example.rsscstllo.kabadges.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

/**
 * Created by rosscastillo on 3/6/17.
 */

public class JSONDownloader extends AsyncTask<Void, Void, String> {

    String jsonUrl;
    Context context;
    ListView listView;
    ProgressDialog progressDialog;

    public JSONDownloader(Context context, String jsonUrl, ListView listView) {
        this.context = context;
        this.jsonUrl = jsonUrl;
        this.listView = listView;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Downloading JSON...");
        progressDialog.setMessage("Please wait.");
        progressDialog.show();
    }

    @Override
    protected String doInBackground(Void... voids) {
        Object connection = Connector.connect(jsonUrl);
        if (connection.toString().startsWith("Error")) {
            return connection.toString();
        }
        try {
            HttpURLConnection urlConnection = (HttpURLConnection) connection;
            if (urlConnection.getResponseCode() == urlConnection.HTTP_OK) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                StringBuffer jsonData = new StringBuffer();
                while ((line = bufferedReader.readLine()) != null) {
                    jsonData.append(line + "\n");
                }
                bufferedReader.close();
                inputStream.close();
                return jsonData.toString();
            } else {
                return "Error " + urlConnection.getResponseMessage();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "Error " + e.getMessage();
        }
    }

    @Override
    protected void onPostExecute(String jsonData) {
        super.onPostExecute(jsonData);
        progressDialog.dismiss();
        if (jsonData.startsWith("Error")) {
            String error = jsonData;
            Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
        } else {
            new JSONParser(context, jsonData, listView).execute();
        }
    }

}
