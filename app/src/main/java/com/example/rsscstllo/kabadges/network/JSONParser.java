package com.example.rsscstllo.kabadges.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.rsscstllo.kabadges.activities.BadgeDetailActivity;
import com.example.rsscstllo.kabadges.adapters.BadgeAdapter;
import com.example.rsscstllo.kabadges.models.Badge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by rosscastillo on 3/6/17.
 */

public class JSONParser extends AsyncTask<Void, Void, Boolean> {

    Context context;
    ProgressDialog progressDialog;
    String jsonData;
    ListView listView;
    ArrayList<Badge> badges = new ArrayList<>();

    public JSONParser(Context context, String jsonData, ListView listView) {
        this.context = context;
        this.jsonData = jsonData;
        this.listView = listView;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Parsing JSON...");
        progressDialog.setMessage("Please wait.");
        progressDialog.show();
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            JSONArray ja = new JSONArray(jsonData);
            JSONObject jo;
            badges.clear();
            for (int i = 0; i < ja.length(); i++) {
                jo = ja.getJSONObject(i);
                if (jo.getInt("badge_category") == 5) {
                    String name = jo.getString("description");
                    String description = jo.getString("translated_safe_extended_description");
                    String smallIconURL = jo.getJSONObject("icons").getString("email");
                    String largeIconURL = jo.getJSONObject("icons").getString("large");
                    ArrayList<String> iconUrls = new ArrayList<>();
                    iconUrls.add(smallIconURL);
                    iconUrls.add(largeIconURL);
                    Badge badge = new Badge(name, description, iconUrls);
                    badges.add(badge);
                }
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean isParsed) {
        super.onPostExecute(isParsed);
        progressDialog.dismiss();
        if (isParsed) {
            BadgeAdapter adapter = new BadgeAdapter(context, badges);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Badge selectedBadge = badges.get(position);
                    Intent detailIntent = new Intent(context, BadgeDetailActivity.class);
                    detailIntent.putExtra("url", selectedBadge.getIconUrls().get(1));
                    detailIntent.putExtra("name", selectedBadge.getName());
                    detailIntent.putExtra("description", selectedBadge.getDescription());
                    context.startActivity(detailIntent);
                }
            });
        } else {
            Toast.makeText(context, "Unable To Parse", Toast.LENGTH_SHORT).show();
        }
    }

}
