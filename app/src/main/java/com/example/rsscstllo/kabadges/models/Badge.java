package com.example.rsscstllo.kabadges.models;

import java.util.ArrayList;

/**
 * Created by rosscastillo on 3/6/17.
 */

public class Badge {

    private String name;
    private String description;
    private ArrayList<String> iconUrls;

    public Badge(String name, String decription, ArrayList<String> iconUrls) {
        this.name = name;
        this.description = decription;
        this.iconUrls = iconUrls;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<String> getIconUrls() {
        return iconUrls;
    }

    public void setIconUrls(ArrayList<String> iconUrls) {
        this.iconUrls = iconUrls;
    }
}
