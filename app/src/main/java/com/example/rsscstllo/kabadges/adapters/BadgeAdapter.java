package com.example.rsscstllo.kabadges.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rsscstllo.kabadges.R;
import com.example.rsscstllo.kabadges.models.Badge;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by rosscastillo on 3/6/17.
 */

public class BadgeAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<Badge> badges;

    public BadgeAdapter(Context context, ArrayList<Badge> badges) {
        this.context = context;
        this.badges = badges;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return badges.size();
    }

    @Override
    public Object getItem(int position) {
        return badges.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BadgeAdapter.ViewHolder holder;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_item_badge, parent, false);
            holder = new BadgeAdapter.ViewHolder();
            holder.badgeIconImageView = (ImageView) convertView.findViewById(R.id.badge_list_thumbnail);
            holder.badgeNameTextView = (TextView) convertView.findViewById(R.id.badge_list_name);
            convertView.setTag(holder);
        } else {
            holder = (BadgeAdapter.ViewHolder) convertView.getTag();
        }

        ImageView badgeIconImageView = holder.badgeIconImageView;
        TextView badgeNameTextView = holder.badgeNameTextView;

        Badge badge = (Badge) getItem(position);
        Picasso.with(context)
                .load(badge.getIconUrls().get(0))
                .placeholder(R.mipmap.ic_launcher_round)
                .into(badgeIconImageView);
        badgeNameTextView.setText(badge.getName());
        badgeNameTextView.setTextColor(Color.argb(179, 0, 0, 0));

        convertView.setBackgroundColor(Color.WHITE);
        return convertView;
    }

    private static class ViewHolder {
        private ImageView badgeIconImageView;
        private TextView badgeNameTextView;
    }

}
